const expect = require('chai').expect;

const {Builder, By, Key, Until, WebElement, Browser} = require('selenium-webdriver');
const firefox = require('selenium-webdriver/firefox')

const BASE_URL = "http://niisku.lab.fi/~kardev/projects/color-picker";
const sleep = (seconds) => new Promise((resolve) => setTimeout(resolve, seconds * 1000));

describe("UI Tests", () => {
    /** @type ThenableWebDriver */
    let driver = undefined;

    before(async () => {
        const options = new firefox.Options();
        options.setBinary("C:\\Program Files\\Mozilla Firefox\\firefox.exe");
        driver = await new Builder()
            .forBrowser(Browser.FIREFOX)
            .setFirefoxOptions(options)
            .build();
        await driver.get(BASE_URL);
        await sleep(1);
    });
    it('Can find all rgb sliders', async () => {
        const slider_r = await driver.findElement(By.id('slider-r'));
        const slider_g = await driver.findElement(By.id('slider-g'));
        const slider_b = await driver.findElement(By.id('slider-b'));
    });
    it('Can find all rgb inputs', async () => {
        const value_r = await driver.findElement(By.id("value-r"));
        const value_g = await driver.findElement(By.id("value-g"));
        const value_b = await driver.findElement(By.id("value-b"));
    });
    it('Can get a hex value from "value-hex" input field', async () => {
        const hex = await driver.findElement(By.id("value-hex")).getAttribute('value');
        expect(hex).to.eq("#00ff00");
    });
    it('Can get and set "value-rgb" input field', async () => {
        const box_rgb = await driver.findElement(By.id("value-rgb"));
        let rgb_value = await box_rgb.getAttribute('value');
        expect(rgb_value).to.eq('rgb(0, 255, 0)');

        // Modify the red value
        const key_stroke_sequence = [];
        for (let i = 0; i < rgb_value.length; i++) {
            key_stroke_sequence.push(Key.BACK_SPACE);
        }
        key_stroke_sequence.push("rgb(255, 255, 0)");
        key_stroke_sequence.push(Key.ENTER);
        await box_rgb.sendKeys(...key_stroke_sequence);

        rgb_value = await box_rgb.getAttribute('value');
        expect(rgb_value).to.eq('rgb(255, 255, 0)');
    });
    it('Can move sliders', async () => {
        const slider_r = await driver.findElement(By.id('slider-r'));
        const slider_g = await driver.findElement(By.id('slider-g'));
        const slider_b = await driver.findElement(By.id('slider-b'));

        await slider_r.sendKeys(Key.LEFT.repeat(300));
        await slider_g.sendKeys(Key.LEFT.repeat(300));
        await slider_b.sendKeys(Key.LEFT.repeat(300));
        await sleep(1);
    });
    after(async () => {
        await driver.close();
    });
});
